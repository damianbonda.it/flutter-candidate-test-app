# Flutter-Candidate-Test-App

## ✅ Development Environment Requirements

- [Flutter SDK](https://flutter.dev/docs/get-started/install)
- [Android Studio](https://developer.android.com/studio) is recommended for the main development IDE and the following plugins are required
    - https://plugins.jetbrains.com/plugin/9212-flutter/
    - https://plugins.jetbrains.com/plugin/6351-dart/
- [XCode](https://developer.apple.com/xcode)

## :computer: Git

**How to enable pre-commit githook**

```bash
git config core.hooksPath .githooks/
cd .githooks
chmod +x pre-commit.sh
```

Commit messages are written the following way [Semantic Commit Messages Guide](https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716)

**Workflow**

1. **git checkout development**
2. **git pull origin development** (the development branch always needs to be up-to-date)
3. **git checkout -b feature/DEV + issue/some-descriptive-name**
4. Do some coding magic...
5. **git push -u origin feature/DEV + issue/some-descriptive-name**
6. Repeat step 1. & 2. and create a new feature branch

## :iphone: Recommended Test Devices

**Android**

- Nexus 4 Android 6 x86 (4.7" 768x1280 xhdpi)
- Pixel 3 Android 9 x86 (5.5" 1080x2160 xxhdpi)
- Pixel 4 XL Android 11 x86 (6.3" 1440x3040 xxxhdpi)

**iOS**

- iPhone SE iOS 14
- iPhone 13 iOS 15
- iPhone 13 Pro Max iOS 15

## :book: Learning Materials

- [Blueprint project which was used as an architecture inspiration](https://github.com/wasabeef/flutter-architecture-blueprints)
- [Crash course](https://fluttercrashcourse.com)
- [Newsletter](https://flutterweekly.net)

**Documentation**

- [https://flutter.io/](https://flutter.io/)
- [https://www.dartlang.org/](https://www.dartlang.org/)

**Youtube channels**

- https://www.youtube.com/channel/UCSIvrn68cUk8CS8MbtBmBkA
- https://www.youtube.com/channel/UCwXdFgeE9KYzlDdR7TG9cMw
