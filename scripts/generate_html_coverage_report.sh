#!/usr/bin/env bash

file=test/coverage_helper_test.g.dart
echo "// Helper file to make coverage work for all dart files\n" > $file
echo "// ignore_for_file: unused_import" > $file
find lib -not -name '*.g.dart' -and -name '*.dart' | cut -c4- | awk -v package=glucose '{printf "import '\''package:%s%s'\'';\n", package, $1}' >> $file
echo "void main(){}" >> $file

lcov --remove coverage/lcov.info 'lib/generated/*' 'lib/src/api/graphql/*' 'lib/database/database.g.dart' -o coverage/new_lcov.info
genhtml coverage/new_lcov.info -o coverage